
# Prueba Final WebdriverIO

*Este proyecto es para la prueba final del curso de WebdriverIO en Abstracta Academy*

## Comenzando

### Pre-requisitos

Las cosas que necesitas tener antes de comenzar son: 

> - Java
> - Allure
> - Node.js
> - Node Package Manager (npm)
> - Visual Studio Code

### Ejecutar las pruebas 

Para ejecutar las pruebas, abrimos una nueva terminal y nos aseguramos que estamos en la carpeta del proyecto. 
Ejecutamos el comando 

```
npm run test
```

Esto va a abrir una ventana en chrome y va a ejecutar todos los tests establecidos en store.js. 